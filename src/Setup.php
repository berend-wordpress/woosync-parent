<?php
/**
 * Setup Class, different parts of our plug-in will be loaded here + some basic functions.
 *
 */
defined( 'ABSPATH' ) OR exit;

class Setup {
	protected static $instance;

	/**
	 * @return mixed
	 */
	public static function init() {
		is_null( self::$instance ) AND self::$instance == new self;

		return self::$instance;
	}

	/**
	 * Setup constructor.
	 */
	public function __construct() {
		add_action( "admin_enqueue_scripts", array( "Setup", "enqueueScripts" ) );
		WoocommerceAttribute::init();
		WoocommerceTerm::init();
		WoocommerceProduct::init();
		WoocommerceProductPage::init();
		WooCommerceCategories::init();
		Webshop::init();
		WebshopsPage::init();
		ChildApi::init();
		WooCommerceOrder::init();
		add_action( 'admin_notices', array( "Setup", "adminNotice" ) );
		//add_submenu_page("woocommerce", "Reset WooSync", "reset-plugin", "administrator", "reset-plugin", array("Setup",  "restePlugin"));

	}

	/**
	 * Resets the Entire plug-in.
	 */
	public static function resetPlugin() {
	    global $wpdb; /** @var $wpdb wpdb */

	    $wpdb->query("TRUNCATE" . $wpdb->prefix . "woocommerce_child_relationships");
		$wpdb->query("TRUNCATE" . $wpdb->prefix . "woocommerce_child_field_changes");

		ob_start();
        ?>

        Tables reset!

		<?php return ob_get_clean();
    }

	/**
	 * When a webshop is wrongly configured we show an admin notice in the wp_admin so users know whats up.
	 */
	public static function adminNotice() {
		if ( get_option( "show_admin_notice" ) == "1" ) {
			?>
            <div class="error notice">
                <p><?php _e( "We can't connect to your recently configured webshop, please check if the API keys and URL are correct.", "comc" ); ?></p>
            </div>
			<?php
		} else {

		}
	}

	/**
	 * Function to enqueue our custom-js. We mostly work with backend stuff so we dont need much CSS and js.
	 */
	public static function enqueueScripts() {
		wp_enqueue_script( "custom-js", get_site_url() . "/wp-content/plugins/woosync-parent/js/products.js" );
	}

	/**
	 * When the plug-in is activated, we need to create two database tables.
	 *
	 * - Child relationships to keep track of all products/attributes/terms on each child website.
	 * - Field changed to keep track on any fields locally changed on a child website.
	 */
	public static function activate() {
		if ( ! current_user_can( 'activate_plugins' ) ) {
			wp_die( __( "You are not allowed to activate plugins", "comc" ) );
		}
		if ( ! self::checkRequiredPlugins() ) {
			wp_die( __( "Please check if you have WooCommerce enabled and the cURL extension for PHP installed.", "comc" ) );
		}
		global $wpdb;
		$charset_collate = $wpdb->get_charset_collate();
		$table1          = "CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "woocommerce_child_field_changes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `webshop_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `field_name` varchar(100) NOT NULL,
  `child_value` varchar(900) NOT NULL,
  PRIMARY KEY (`ID`)
) {$charset_collate};";
		$table2          = "CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "woocommerce_child_relationships` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `child_id` int(11) NOT NULL,
  `sub_website_id` int(11) NOT NULL,
  `type` enum('product','attribute','term') COLLATE utf8mb4_unicode_ci NOT NULL,
  `variation_parent_id` int(11) DEFAULT NULL,
  `is_edited` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`)
) {$charset_collate};";
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $table1 );
		dbDelta( $table2 );
	}

	/**
	 * Emtpy function for deactivation.
	 */
	public static function deactivate() {
		if ( ! current_user_can( 'activate_plugins' ) ) {
			wp_die( __( "You are not allowed to activate plugins", "comc" ) );
		}
	}

	/**
	 * Emtpy function for uninstallation, might need future cleanup.
	 */
	public static function uninstall() {
		if ( ! current_user_can( 'activate_plugins' ) ) {
			wp_die( __( "You are not allowed to activate plugins", "comc" ) );
		}
	}

	/**
	 * Checks whether or not WooCommerce is installed.
	 *
	 * @return bool
	 */
	private static function checkRequiredPlugins() {
		if ( ! is_plugin_active( "woocommerce/woocommerce.php" ) ) {
			return false;
		}
		if ( ! function_exists( "curl_version" ) ) {
			return false;
		}

		return true;
	}
}