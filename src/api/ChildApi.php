<?php
/**
 * This class adds an extra api route to the WooCommerce Api, it makes it possible for the child webshop to post local
 * changes to the parent webshop. For example if the regular price is changed on a child website, we do not always
 * want to overwrite these changes when we edit the product on the parent webshop. Thus we check which fields are
 * changed on the child and won't push these particular fields.
 *
 * There will be an option where we can reset products so even when the child has changed fields these changes will
 * be overwritten.
 *
 */
defined( 'ABSPATH' ) OR exit;

class ChildApi {
	protected static $instance;

	public static function init() {
		is_null( self::$instance ) AND self::$instance == new self;

		return self::$instance;
	}

	public function __construct() {
		add_action( 'rest_api_init', function() {
			register_rest_route( "wc/v2", "/products/(?P<product_id>\d+)/fields", array(
				'methods'  => 'PUT',
				'callback' => array( "ChildApi", "insertFieldEdit" ),
			) );
			register_rest_route( "wc/v2", "/products/(?P<product_id>\d+)/(?P<reduce>\d+)/reducestock", array(
				'methods'  => 'PUT',
				'callback' => array( "ChildApi", "reduceStock" ),
			) );
		} );
	}

	public static function reduceStock( $data ) {
		/** @var $data WP_REST_Request */
		$params    = $data->get_params();
		$productId = $params["product_id"];
		$quantity  = $params["reduce"];
		$webShops  = Webshop::getAllWebshops();
		foreach ( $webShops as $webshop ) {
			$parentProductId = WoocommerceChildRelationships::getParentProductIdByChildId( $productId, $webshop->ID, "product" );
			if ( $parentProductId == null ) {
				continue;
			} else {
				$skipWebshop = $webshop->ID;
				break;
			}
		}
		if ( isset( $parentProductId ) && isset( $skipWebshop ) ) {
			$product = wc_get_product($parentProductId);
			wc_update_product_stock($product, $quantity, "decrease");
			foreach ( $webShops as $webshop ) {
				if ( $webshop->ID == $skipWebshop ) {
					continue;
				}
				if ( WoocommerceChildRelationships::doesExistOnChild( $parentProductId, $webshop->ID, "product" ) ) {
					$wooApi = new WoocommerceApi($webshop->ID);
					$childProductId = WoocommerceChildRelationships::getChildRelationshipId($parentProductId, $webshop->ID, "product");
					$returnData = $wooApi->post("put", "products/" . $childProductId . "/" . $quantity . "/reducestock", null);
				}

			}
		}
	}

	/**
	 * Only two fields can be changed on a child webshop, maybe more fields will be added later.
	 *
	 * @param $data
	 *
	 * @return string
	 */
	public static function insertFieldEdit( $data ) {
		/** @var $data WP_REST_Request */
		$params            = $data->get_params();
		$productChildId    = $params["product_id"];
		$webshopUrl        = $params["siteUrl"];
		$webshopId         = Webshop::getWebshopByUrl( $webshopUrl );
		$parentProductId   = WoocommerceChildRelationships::getParentProductIdByChildId( $productChildId, $webshopId );
		$parentProduct     = wc_get_product( $parentProductId );
		$parentProductData = $parentProduct->get_data();
		if ( $parentProductData["description"] != $params["description"] ) {
			WoocommerceFieldChanges::saveOrUpdateFieldChange( $webshopId, $parentProductId, "description", $params["description"] );
		}
		if ( $parentProductData["short_description"] != $params["short_description"] ) {
			WoocommerceFieldChanges::saveOrUpdateFieldChange( $webshopId, $parentProductId, "short_description", $params["short_description"] );
		}

		return "It works?????";
	}
}