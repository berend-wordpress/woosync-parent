<?php
/**
 * This is a wrapper class for the WooCommerce Api, we would like to catch any exceptions and show the message using
 * WP_DIE, this makes debugging easier for us and our customers.
 */
defined( 'ABSPATH' ) OR exit;
use Automattic\WooCommerce\Client;

class WoocommerceApi {
	private $wooClient;
	private $webshop;

	/**
	 * WoocommerceApi constructor.
	 *
	 * @param $webshopId
	 */
	public function __construct( $webshopId ) {
		$this->setWebshop( new Webshop( $webshopId ) );
		$wooClient = new Client( $this->getWebshop()->getWebshopUrl(), $this->getWebshop()->getConsumerKey(), $this->getWebshop()->getConsumerSecret(), array(
			"wp_api"            => true,
			"version"           => "wc/v2",
			"query_string_auth" => true
		) );
		$this->setWooClient( $wooClient );
	}

	/**
	 * Before we post the request to any child webshop we validate if any of the fields are changed on the child
	 * website, if so we wont push changes unless it is forced by a reset function.
	 *
	 * @param      $type
	 * @param      $url
	 * @param      $data
	 * @param bool $parent_id
	 *
	 * @return null
	 */
	public function post( $type, $url, $data, $parent_id = false ) {
		if ( $parent_id != false ) {
			$data = WoocommerceFieldChanges::validateChangedFields( $data, $parent_id, $this->getWebshop()->getId() );
		}
		$returnData = null;
		try {
			switch ( $type ) {
				case "post":
					$returnData = $this->getWooClient()->post( $url, $data );
					break;
				case "put":
					$returnData = $this->getWooClient()->put( $url, $data );
					break;
				case "delete":
					$returnData = $this->getWooClient()->delete( $url, array( "force" => true ) );
					break;
				case "trash":
					$returnData = $this->getWooClient()->delete( $url );
					break;
			}

		} catch ( Exception $e ) {
			wp_die( $e->getMessage() );
		}

		return $returnData;

	}

	/**
	 * We use this test function when a new webshop is added. We want a different error message shown on screen then
	 * the default message given in the post function in this class. Besided that we need to set admin notices and
	 * save the webshop ID from which the configuration is not correct.
	 *
	 * @param $type
	 * @param $url
	 * @param $data
	 *
	 * @return null
	 */
	public function testPost( $type, $url, $data ) {
		$returnData = null;
		try {
			switch ( $type ) {
				case "post":
					$returnData = $this->getWooClient()->post( $url, $data );
					break;
				case "put":
					$returnData = $this->getWooClient()->put( $url, $data );
					break;
				case "delete":
					$returnData = $this->getWooClient()->delete( $url, array( "force" => true ) );
					break;
				case "trash":
					$returnData = $this->getWooClient()->delete( $url );
					break;
			}
		} catch ( Exception $e ) {
			update_option( "show_admin_notice", "1" );
			update_option( "show_admin_notice_d", $this->getWebshop()->getId() );
			if ( isset( $this->webshop->webshopName ) ) {
				update_option( "webshop_admin_notice", $this->webshop->webshopName );
			} else {
				update_option( "webshop_admin_notice", "Webshop" );
			}
			wp_die( __( "There is somethnig wrong in your configuration, please check both API keys and the website URL again.", "comc" ) );
		}

		return $returnData;
	}

	/**
	 * @return mixed
	 */
	public function getWebshop() {
		return $this->webshop;
	}

	/**
	 * @param mixed $webshop
	 */
	private function setWebshop( $webshop ) {
		$this->webshop = $webshop;
	}

	/**
	 * @return mixed
	 */
	public function getWooClient() {
		return $this->wooClient;
	}

	/**
	 * @param mixed $wooClient
	 */
	private function setWooClient( $wooClient ) {
		$this->wooClient = $wooClient;
	}
}
