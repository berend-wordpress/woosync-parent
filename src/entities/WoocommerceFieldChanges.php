<?php

/**
 * This entity is used to modify the woocommerce_field_changes table. This table keeps track of all the fields
 * manually changed on each child website for each product. For now only the post description is supported. However
 * in the future we are looking to expand on this functionally and gives users the ability to change any fields.
 */
class WoocommerceFieldChanges {
	protected static $instance;

	public static function init() {
		is_null( self::$instance ) AND self::$instance == new self;

		return self::$instance;
	}

	/**
	 * Small wrapper function which either updates an existing row, or inserts a new one. To prevent to many if-else
	 * statements in the code.
	 *
	 * @param $webshop_id
	 * @param $parent_id
	 * @param $field_name
	 * @param $child_value
	 */
	public static function saveOrUpdateFieldChange( $webshop_id, $parent_id, $field_name, $child_value ) {
		if ( self::isFieldChanged( $webshop_id, $parent_id, $field_name ) ) {
			self::updateFieldChange( $webshop_id, $parent_id, $field_name, $child_value );
		} else {
			self::saveFieldChange( $webshop_id, $parent_id, $field_name, $child_value );
		}
	}

	/**
	 * Saves a changed field on the child webshop.
	 *
	 * @param $webshop_id
	 * @param $parent_id
	 * @param $field_name
	 * @param $child_value
	 *
	 * @return false|int
	 */
	public static function saveFieldChange( $webshop_id, $parent_id, $field_name, $child_value ) {
		global $wpdb;
		$returnStatus = $wpdb->insert( $wpdb->prefix . "woocommerce_child_field_changes", array(
			"webshop_id"  => $webshop_id,
			"parent_id"   => $parent_id,
			"field_name"  => $field_name,
			"child_value" => $child_value
		) );

		return $returnStatus;
	}

	/**
	 * When an already locally changed field is updated on the child, use this function to save it.
	 *
	 * @param $webshop_id
	 * @param $parent_id
	 * @param $field_name
	 * @param $child_value
	 *
	 * @return false|int
	 */
	public static function updateFieldChange( $webshop_id, $parent_id, $field_name, $child_value ) {
		global $wpdb;
		$returnStatus = $wpdb->update( $wpdb->prefix . "woocommerce_child_field_changes", array( "child_value" => $child_value ), array(
			"webshop_id" => $webshop_id,
			"parent_id"  => $parent_id,
			"field_name" => $field_name
		) );

		return $returnStatus;
	}

	/**
	 * When a product is reset, the field change is overwritten thus deleted.
	 *
	 * @param $webshop_id
	 * @param $parent_id
	 * @param $field_name
	 *
	 * @return false|int
	 */
	public static function delelteFieldChange( $webshop_id, $parent_id, $field_name ) {
		global $wpdb;
		$returnStatus = $wpdb->delete( $wpdb->prefix . "woocommerce_child_field_changes", array(
			"webshop_id" => $webshop_id,
			"parent_id"  => $parent_id,
			"field_name",
			$field_name
		) );

		return $returnStatus;
	}

	/**
	 * Simple function to check whether or not a field is changed locally on a child.
	 *
	 * @param $webshop_id
	 * @param $parent_id
	 * @param $field_name
	 *
	 * @return bool
	 */
	public static function isFieldChanged( $webshop_id, $parent_id, $field_name ) {
		global $wpdb;
		$result = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM " . $wpdb->prefix . "woocommerce_child_field_changes WHERE `webshop_id`=%d AND `parent_id`=%d AND `field_name`=%s", $webshop_id, $parent_id, $field_name ) );
		if ( empty( $result ) ) {
			return false;
		}

		return true;
	}

	/**
	 * For now only two fields can be changed on child webshop without it being overwritten by the parent webshop. Maybe
	 * In the future more fields will be added.
	 *
	 * @param $data
	 * @param $parent_product_id
	 * @param $webshop_id
	 *
	 * @return mixed
	 */
	public static function validateChangedFields( $data, $parent_product_id, $webshop_id ) {
		if ( self::isFieldChanged( $webshop_id, $parent_product_id, "description" ) ) {
			unset( $data["description"] );
		}
		if ( self::isFieldChanged( $webshop_id, $parent_product_id, "short_description" ) ) {
			unset( $data["short_description"] );
		}

		return $data;

	}

	/**
	 * Reset the entire product, so all fields need to be removed.
	 *
	 * @param $webshop_id
	 * @param $parent_id
	 *
	 * @return false|int
	 */
	public static function resetProduct( $webshop_id, $parent_id ) {
		global $wpdb;
		$result = $wpdb->delete( $wpdb->prefix . "woocommerce_child_field_changes", array(
			"webshop_id" => $webshop_id,
			"parent_id"  => $parent_id
		) );

		return $result;
	}
}