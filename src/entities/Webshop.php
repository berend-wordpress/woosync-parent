<?php

/**
 * The Webshop Entity.
 *
 * All actions related to changing a webshop entitiy are located here. We register the necessary actions, register
 * the webshop post type and add it to the WooCommerce Menu. We have actions for both deleting and trashing a webshop
 * pointing to the same function. This is because we need to know if people are deleting a webshop with a wrong
 * configuration.
 */
class Webshop {
	private $consumerKey;
	private $consumerSecret;
	private $webshopUrl;
	private $webshopName;
	private $prijsMarge;
	private $ID;

	/**
	 * Init.
	 */
	public static function init() {
		register_post_type( "webshop", array(
			"labels"       => array(
				'name'               => _x( 'Webshops', 'Admin panel', 'comc' ),
				'singular_name'      => _x( 'webshop', 'Admin panel', 'comc' ),
				'menu_name'          => _x( 'Connected webshops', 'Admin sub-menu item', 'comc' ),
				'name_admin_bar'     => _x( 'Webshops', 'Admin panel', 'comc' ),
				'add_new'            => _x( 'New webshop', 'Admin button', 'comc' ),
				'add_new_item'       => _x( 'Add new webshop', 'Admin panel', 'comc' ),
				'new_item'           => _x( 'New webshop', 'Admin panel', 'comc' ),
				'edit_item'          => _x( 'Edit webshop', 'Admin panel', 'comc' ),
				'view_item'          => _x( 'View webshop', 'Admin panel', 'comc' ),
				'all_items'          => _x( 'Connected webshops', 'Admin panel', 'comc' ),
				'search_items'       => _x( 'Search webshops', 'Admin panel', 'comc' ),
				'parent_item_colon'  => _x( 'Parent webshop', 'Admin panel', 'comc' ),
				'not_found'          => _x( 'No Webshops found', 'Admin panel', 'comc' ),
				'not_found_in_trash' => _x( 'No Webshops found in trash', 'Admin panel', 'comc' ),
			),
			"public"       => true,
			"has_archive"  => true,
			"taxonomies"   => array( "product_cat" ),
			"show_ui"      => true,
			"show_in_menu" => "woocommerce"
		) );
		add_action( "save_post", array( "Webshop", "saveWebsite" ), 10, 2 );
		add_action( "pre_post_update", array( "Webshop", "beforeSaveWebsite" ), 10, 2 );
		add_action( "before_delete_post", array( "Webshop", "deleteWebshop" ), 10 );
		add_action( "wp_trash_post", array( "Webshop", "deleteWebshop" ) );
	}

	/**
	 * Webshop constructor.
	 *
	 * @param $id
	 */
	public function __construct( $id ) {
		$post           = get_post( $id );
		$webshopUrl     = get_post_meta( $id, "webshopUrl" );
		$consumerKey    = get_post_meta( $id, "consumerKey" );
		$consumerSecret = get_post_meta( $id, "consumerSecret" );
		$prijsMarge     = get_post_meta( $id, "prijsMarge" );
		$this->setConsumerSecret( $consumerSecret[0] );
		$this->setConsumerKey( $consumerKey[0] );
		$this->setWebshopUrl( $webshopUrl[0] );
		$this->setWebshopName( $post->post_title );
		$this->setPrijsMarge( $prijsMarge[0] );
		$this->ID = $id;
	}

	/**
	 * When a webshop is deleted or trashed we check if it was a wrongly configured webshop. If so we disable the
	 * admin notice visible at the top of the screen.
	 *
	 * @param $id
	 */
	public static function deleteWebshop( $id ) {
		$post = get_post( $id );
		if ( $post->post_type == "webshop" ) {
			$errorWebShopId = get_option( "show_admin_notice_d" );
			if ( $errorWebShopId == $id ) {
				update_option( "show_admin_notice", "0" );
			}
		}
	}

	/**
	 * This function is called before the webshop is actually saved. Because we need to know in which categories this
	 * webshop was added and from which one they were deleted.
	 *
	 * @param $postId
	 * @param $post
	 */
	public static function beforeSaveWebsite( $postId, $post ) {
		if ( $post["post_type"] == 'webshop' ) {
			$newCats = $_POST["tax_input"]["product_cat"];
			$oldCats = get_the_terms( $postId, "product_cat" );
			foreach ( $newCats as $catId ) {
				if ( $catId != "0" ) {
					$products = get_posts( array(
						"post_type" => "product",
						"tax_query" => array(
							array(
								"taxonomy" => "product_cat",
								"field"    => "term_id",
								"terms"    => $catId,
							)
						)
					) );
					foreach ( $products as $product ) {
						if ( ! WoocommerceChildRelationships::doesExistOnChild( $product->ID, $postId ) ) {
							WoocommerceProduct::preparePostDataAndSavePost( array( "webshop" => $postId ), array( $product->ID ) );
						}
					}
				}
			}
			if ( $oldCats != false ) {
				foreach ( $oldCats as $cat ) {
					if ( ! in_array( $cat->term_id, $newCats ) ) {
						$toBeRemovedProducts = get_posts( array(
							"post_type" => "product",
							"tax_query" => array(
								array(
									"taxonomy" => "product_cat",
									"field"    => "term_id",
									"terms"    => $cat->term_id
								)
							)
						) );
						foreach ( $toBeRemovedProducts as $toBeRemovedProduct ) {
							if ( WoocommerceChildRelationships::doesExistOnChild( $toBeRemovedProduct->ID, $postId ) ) {
								$productChildId = WoocommerceChildRelationships::getChildRelationshipId( $toBeRemovedProduct->ID, $postId, "product" );
								$wooApi         = new WoocommerceApi( $postId );
								$wooApi->post( "delete", "products/{$productChildId}", null );
								WoocommerceProduct::deleteAllVariations( $toBeRemovedProduct->ID, $postId );
								WoocommerceFieldChanges::resetProduct( $postId, $toBeRemovedProduct->ID );
								WoocommerceChildRelationships::deleteChildRelationship( $toBeRemovedProduct->ID, $postId, "product" );
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Saves the post_meta and validates the configured API keys and webshop URL.
	 *
	 * @param $postId
	 * @param $post
	 */
	public static function saveWebsite( $postId, $post ) {
		if ( $post->post_type == 'webshop' ) {
			if ( isset( $_POST["consumerKey"] ) ) {
				update_post_meta( $postId, "consumerKey", $_POST["consumerKey"] );
			}
			if ( isset( $_POST["consumerSecret"] ) ) {
				update_post_meta( $postId, "consumerSecret", $_POST["consumerSecret"] );
			}
			if ( isset( $_POST["webshopUrl"] ) ) {
				update_post_meta( $postId, "webshopUrl", $_POST["webshopUrl"] );
			}
			if ( isset( $_POST["beheerderNaam"] ) ) {
				update_post_meta( $postId, "beheerderNaam", $_POST["beheerderNaam"] );
			}
			if ( isset( $_POST["beheerderEmail"] ) ) {
				update_post_meta( $postId, "beheerderEmail", $_POST["beheerderEmail"] );
			}
			if ( isset( $_POST["prijsMarge"] ) ) {
				update_post_meta( $postId, "prijsMarge", $_POST["prijsMarge"] );
			}
		}
		/**
		 * We test if the API keys and webshop URL are correct, if not an exception is thrown by the wooapi. We call
		 * a seperate test function in the wooapi class so we can correctly show the error message.
		 */
		if ( isset( $_POST["consumerSecret"] ) && isset( $_POST["consumerKey"] ) && isset( $_POST["webshopUrl"] ) ) {
			$wooApi              = new WoocommerceApi( $postId );
			$testProduct         = array( "name" => _x( "First test", "Test product title", "comc" ) );
			$returnData          = $wooApi->testPost( "post", "products", $testProduct );
			$toBeDeltedProductId = $returnData["id"];
			$wooApi->post( "delete", "products/{$toBeDeltedProductId}", null, null );
			$errorWebShopId = get_option( "show_admin_notice_d" );
			if ( $errorWebShopId == $postId ) {
				update_option( "show_admin_notice", "0" );
			}
		}
	}

	/**
	 * @param $webshopUrl
	 *
	 * @return null|string
	 */
	public static function getWebshopByUrl( $webshopUrl ) {
		global $wpdb;
		$webshopId = $wpdb->get_var( $wpdb->prepare( "SELECT post_id from " . $wpdb->prefix . "postmeta where `meta_key`='webshopUrl' AND `meta_value` LIKE %s", "%" . $webshopUrl . "%" ) );

		return $webshopId;
	}

	/**
	 * @param $webshopId
	 *
	 * @return Webshop
	 */
	public static function getWebshopById( $webshopId ) {
		$webshop = new Webshop( $webshopId );

		return $webshop;
	}

	/**
	 * @return array
	 */
	public static function getAllWebshops() {
		$query = get_posts( array( "post_type" => "webshop" ) );

		return $query;
	}

	/**
	 * @return mixed
	 */
	public function getPrijsMarge() {
		return $this->prijsMarge;
	}

	/**
	 * @param mixed $prijsMarge
	 */
	public function setPrijsMarge( $prijsMarge ) {
		$this->prijsMarge = $prijsMarge;
	}

	/**
	 * @return mixed
	 */
	public function getWebshopName() {
		return $this->webshopName;
	}

	/**
	 * @param mixed $webshopName
	 */
	public function setWebshopName( $webshopName ) {
		$this->webshopName = $webshopName;
	}

	/**
	 * @return mixed
	 */
	public function getConsumerKey() {
		return $this->consumerKey;
	}

	/**
	 * @param mixed $consumerKey
	 */
	public function setConsumerKey( $consumerKey ) {
		$this->consumerKey = $consumerKey;
	}

	/**
	 * @return mixed
	 */
	public function getConsumerSecret() {
		return $this->consumerSecret;
	}

	/**
	 * @param mixed $consumerSecret
	 */
	public function setConsumerSecret( $consumerSecret ) {
		$this->consumerSecret = $consumerSecret;
	}

	/**
	 * @return mixed
	 */
	public function getWebshopUrl() {
		return $this->webshopUrl;
	}

	/**
	 * @param mixed $webshopUrl
	 */
	public function setWebshopUrl( $webshopUrl ) {
		$this->webshopUrl = $webshopUrl;
	}

	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->ID;
	}
}