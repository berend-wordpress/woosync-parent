<?php

/**
 * This entitiy class is used to modify the woocommerce_child_relationships table. This table keeps track of all
 * products, taxonomies and terms shared with any child webshop. We wrote a few wrapper functions to quickly access the
 * information we need. Functions as doesExistOnChild() for example.
 *
 * WooCommerce is very consistent in using "Attribute" instead of "taxonomy", thus we do the same when saving types
 * in the database. Three types are allowed as you can see in the allowedTypes array.
 *
 * @Author: Berend de Groot
 * @Date: 1-8-17
 * @Time: 16:06
 */
class WoocommerceChildRelationships {
	protected static $instance;
	protected static $allowedTypes = array( "product", "attribute", "term" );

	/**
	 * Init
	 *
	 * @return mixed
	 */
	public static function init() {
		is_null( self::$instance ) AND self::$instance == new self;

		return self::$instance;
	}

	/**
	 * WoocommerceChildRelationships constructor.
	 */
	public function __construct() { }

	/**
	 * @param $parent_id
	 *
	 * @return array|null
	 */
	public static function getSharedWebsites( $parent_id ) {
		global $wpdb;
		$websiteIds = $wpdb->get_results( $wpdb->prepare( "SELECT sub_website_id from " . $wpdb->prefix . "woocommerce_child_relationships WHERE `parent_id` = %d", $parent_id ), ARRAY_N );
		$returnIds  = null;
		foreach ( $websiteIds as $websiteId ) {
			$returnIds[] = $websiteId[0];
		}

		return $returnIds;
	}

	/**
	 * After a product is shared to a child webshop, the relation has to be saved in the database.
	 *
	 * @param     $parent_id - ID on this (parent) webshop
	 * @param     $child_id - ID on the child webshop (after its shared)
	 * @param int $sub_website_id - ID of the website it is shared to.
	 * @param     $type - One of the allowedtypes.
	 * @param int $variation_parent_id - If this product is an variation, link it to its parent.
	 * @param int $is_edited - Is this product locally changed on the child?
	 *
	 * @return bool|false|int
	 */
	public static function saveChildRelationship( $parent_id, $child_id, $sub_website_id = 0, $type, $variation_parent_id = 0, $is_edited = 0 ) {
		global $wpdb;
		$return = false;
		if ( in_array( $type, self::$allowedTypes ) ) {
			$return = $wpdb->insert( $wpdb->prefix . "woocommerce_child_relationships", array(
				"parent_id"           => $parent_id,
				"child_id"            => $child_id,
				"sub_website_id"      => $sub_website_id,
				"type"                => $type,
				"variation_parent_id" => $variation_parent_id,
				"is_edited"           => $is_edited
			) );
		}

		return $return;
	}

	/**
	 * Received the parent product/attribute/term ID by the child ID.
	 *
	 * @param        $childId
	 * @param        $webshop_id
	 * @param string $type
	 *
	 * @return null|string
	 */
	public static function getParentProductIdByChildId( $childId, $webshop_id, $type = "product" ) {
		global $wpdb;
		$parentId = $wpdb->get_var( $wpdb->prepare( "SELECT parent_id from " . $wpdb->prefix . "woocommerce_child_relationships where `child_id`=%d AND `sub_website_id`=%d AND `type`=%s", $childId, $webshop_id, $type ) );

		return $parentId;

	}

	/**
	 * Receives all the (from the parent site) IDs from each variation by the parent ID.
	 *
	 * @param        $variation_parent_id
	 * @param        $webshop_id
	 * @param string $type
	 *
	 * @return null
	 */
	public static function getVariationsByParentId( $variation_parent_id, $webshop_id, $type = "product" ) {
		global $wpdb;
		$variationIds = $wpdb->get_results( $wpdb->prepare( "SELECT parent_id FROM " . $wpdb->prefix . "woocommerce_child_relationships WHERE `variation_parent_id` = %d AND `sub_website_id` = %d AND `type` = %s", $variation_parent_id, $webshop_id, $type ) );
		$returnVars   = null;
		foreach ( $variationIds as $varId ) {
			$returnVars[ $varId->parent_id ] = $varId->parent_id;
		}

		return $returnVars;

	}

	/**
	 * Check whether a product is already shared to a certain child webshop.
	 *
	 * @param $parent_id
	 * @param $webshop_id
	 * @param $type
	 *
	 * @return bool
	 */
	public static function doesExistOnChild( $parent_id, $webshop_id, $type = "product" ) {
		$productChildId = WoocommerceChildRelationships::getChildRelationshipId( $parent_id, $webshop_id, $type );
		if ( $productChildId == null ) {
			return false;
		}

		return true;
	}

	/**
	 * Recieved the child ID by the parent ID.
	 *
	 * @param $parent_id
	 * @param $sub_website_id
	 * @param $type
	 *
	 * @return null|string
	 */
	public static function getChildRelationshipId( $parent_id, $sub_website_id, $type ) {
		global $wpdb;
		$child_id = $wpdb->get_var( $wpdb->prepare( "SELECT child_id FROM " . $wpdb->prefix . "woocommerce_child_relationships WHERE `parent_id` = %d AND `sub_website_id` = %d AND `type` = %s", $parent_id, $sub_website_id, $type ) );

		return $child_id;
	}

	/**
	 * Deletes the relationship when a product is deleted.
	 *
	 * @param $parent_id
	 * @param $sub_website_id
	 * @param $type
	 *
	 * @return false|int
	 */
	public static function deleteChildRelationship( $parent_id, $sub_website_id, $type ) {
		global $wpdb;
		$return = $wpdb->delete( $wpdb->prefix . "woocommerce_child_relationships", array(
			"parent_id"      => $parent_id,
			"sub_website_id" => $sub_website_id,
			"type"           => $type
		) );

		return $return;
	}
}