<?php
/**
 * This class makes changed to the frontend of the Webshops page. It adds the meta boxes, created extra columns and
 * poplulate those columns.
 */
defined( 'ABSPATH' ) OR exit;

class WebshopsPage {
	protected static $instance;
	const CAT_COL = "shared_cats";

	/**
	 * @return mixed
	 */
	public static function init() {
		is_null( self::$instance ) AND self::$instance == new self;

		return self::$instance;
	}

	/**
	 * WebshopsPage constructor.
	 */
	public function __construct() {
		add_action( "add_meta_boxes", array( "WebshopsPage", "registerMetaBoxes" ), 10, 2 );
		add_filter( "manage_edit-webshop_columns", array( "WebshopsPage", "addSharedCatsColumn" ), 100, 1 );
		add_action( "manage_webshop_posts_custom_column", array( "WebshopsPage", "populateSharedCatsColumn" ), 10, 2 );
	}

	/**
	 * Populate the shared categories column.
	 *
	 * @param $column_name
	 * @param $post_id
	 */
	public static function populateSharedCatsColumn( $column_name, $post_id ) {
		if ( $column_name == self::CAT_COL ) {
			$productCats = get_the_terms( $post_id, "product_cat" );
			if ( $productCats != null ) {
				foreach ( $productCats as $cat ) {
					echo sprintf( __( "<a href='%s'>%s</a>, ", "comc" ), get_edit_term_link( $cat->term_id, 'product_cat' ), $cat->name );
				}
			} else {
				echo __( "No shared categories.", "comc" );
			}
		}
	}

	/**
	 * Add a column where the shared categories are displayed.
	 *
	 * @param $columns
	 *
	 * @return mixed
	 */
	public static function addSharedCatsColumn( $columns ) {
		unset( $columns["date"] );
		$columns[ self::CAT_COL ] = __( "Shared categories", "comc" );
		$columns["date"]          = __( "Date", "comc" );

		return $columns;
	}

	/**
	 * Register needed meta boxes.
	 *
	 * @param $postType
	 * @param $post
	 */
	public static function registerMetaBoxes( $postType, $post ) {
		if ( $postType == "webshop" ) {
			add_meta_box( "consumerKey", _x( "Child API Key *", "Connected shops metabox, required", "comc" ), array(
				"WebshopsPage",
				"echoConsumerKeyField"
			), "webshop", "normal", "default" );
			add_meta_box( "consumerSecret", _x( "Child API Secret *", "Connected shops metabox, required", "comc" ), array(
				"WebshopsPage",
				"echoConsumerSecretField"
			), "webshop", "normal", "default" );
			add_meta_box( "webshopUrl", _x( "Webshop URL *", "Connected shops metabox, required", "comc" ), array(
				"WebshopsPage",
				"echoWebshopUrlField"
			), "webshop", "normal", "default" );
			add_meta_box( "beheerderNaam", _x( "Shop owner name", "Connected shops metabox, optional", "comc" ), array(
				"WebshopsPage",
				"echoBeheerderNaamField"
			), "webshop", "normal", "default" );
			add_meta_box( "beheerderEmail", _x( "Shop owner e-mail address", "Connected shops metabox, optional", "comc" ), array(
				"WebshopsPage",
				"echoBeheerderEmailField"
			), "webshop", "normal", "default" );
			add_meta_box( "prijsMarge", _x( "Add percentage to price (example: 10%)", "Connected shops metabox, optional", "comc" ), array(
				"WebshopsPage",
				"echoPrijsMargeField"
			), "webshop", "normal", "default" );
		}
	}

	/**
	 * Small wrapper for displaying fields.
	 *
	 * @param $fieldKey
	 */
	private static function echoField( $fieldKey, $placeholder = "", $class = "regular-text" ) {
		global $post;
		$fieldValue = get_post_meta( $post->ID, $fieldKey, true );
		if ( ! empty( $placeholder ) ) {
			$placeholder = " placeholder=\"$placeholder\"";
		}
		echo "<input type='text' name='$fieldKey' value='{$fieldValue}' class='{$class}'$placeholder>";
	}

	public static function echoPrijsMargeField() {
		self::echoField( "prijsMarge", __( "optional", "comc" ) );
	}

	public static function echoBeheerderNaamField() {
		self::echoField( "beheerderNaam", __( "optional", "comc" ) );
	}

	public static function echoBeheerderEmailField() {
		self::echoField( "beheerderEmail", __( "optional", "comc" ) );
	}

	public static function echoConsumerKeyField() {
		self::echoField( "consumerKey", __( "ck_", "comc" ), "widefat" );
	}

	public static function echoConsumerSecretField() {
		self::echoField( "consumerSecret", __( "cs_", "comc" ), "widefat" );
	}

	public static function echoWebshopUrlField() {
		self::echoField( "webshopUrl", __( "https://", "comc" ), "widefat" );
	}
}
