<?php
/**
 * This class was used to push entire categories of products but was later moved, we might need it later.
 */
defined( 'ABSPATH' ) OR exit;

class WooCommerceCategories {
	protected static $instance;

	/**
	 * @return mixed
	 */
	public static function init() {
		is_null( self::$instance ) AND self::$instance == new self;

		return self::$instance;
	}

	/**
	 * WooCommerceCategories constructor.
	 */
	public function __construct() {
		/*add_filter("bulk_actions-edit-product_cat", array("WooCommerceCategories", "addBulkPushToWebshops"), 10, 1);
		add_filter("handle_bulk_actions-edit-product_cat", array("WooCommerceCategories", "handleBulkPushToWebshops"), 10, 3);
		*/
	}

	/**
	 * @param $bulk_actions
	 *
	 * @return mixed
	 */
	public static function addBulkPushToWebshops( $bulk_actions ) {
		$bulk_actions["cat_bulk_push"] = __( "Push to Webshop", "comc" );

		return $bulk_actions;
	}

	/**
	 * @param $redirect_to
	 * @param $doaction
	 * @param $post_ids
	 *
	 * @return mixed
	 */
	public static function handleBulkPushToWebshops( $redirect_to, $doaction, $post_ids ) {
		if ( $doaction == "cat_bulk_push" ) {
			$x = 10;
		}

		return $redirect_to;
	}
}