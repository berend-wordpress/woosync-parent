<?php
/**
 * This class handles all the changes made to the product overview. What we do?
 *
 * - Create a new column called shared webshops to dipslay on which webshops the product is shared.
 * - Add a new bulk action called "Push to webshops" so multiple products can be selected and pushed.
 * - Add the webshops columns to the bulk edit function.
 * - Adds a reset button to each shared product so locally changed fields on the child can be overwritten.
 */
defined( 'ABSPATH' ) OR exit;

class WoocommerceProductPage {
	const DO_ACTION = "push";
	const WEB_COL   = "webshops";
	protected static $instance;

	/**
	 * @return mixed
	 */
	public static function init() {
		is_null( self::$instance ) AND self::$instance == new self;

		return self::$instance;
	}

	/**
	 * WoocommerceProductPage constructor.
	 */
	public function __construct() {
		add_action( "add_meta_boxes", array( "WoocommerceProductPage", "registerMetaBoxes" ), 10, 2 );
		add_filter( "manage_edit-product_columns", array( "WoocommerceProductPage", "addFieldsProductTable" ), 100, 1 );
		add_action( "manage_product_posts_custom_column", array(
			"WoocommerceProductPage",
			"populateWebshopsField"
		), 10, 2 );
		add_action( "bulk_edit_custom_box", array( "WoocommerceProductPage", "addBulkEditWebshops" ), 10, 2 );
		add_filter( "bulk_actions-edit-product", array( "WoocommerceProductPage", "addBulkPushToWebshops" ), 10, 1 );
		add_filter( "handle_bulk_actions-edit-product", array(
			"WoocommerceProductPage",
			"handleBulkPushToWebshops"
		), 10, 3 );
		add_filter( "post_row_actions", array( "WoocommerceProductPage", "addCustomRowActions" ), 10, 2 );
	}

	/**
	 * Adds the reset button to each shared product.
	 *
	 * @param $actions
	 * @param $post
	 *
	 * @return mixed
	 */
	public static function addCustomRowActions( $actions, $post ) {
		if ( $post->post_type == "product" ) {
			$actions["reset"] = '<a href="' . get_site_url() . '/wp-content/plugins/woocommerce-parent/post.php?post=' . $post->ID . '&action=reset">' . _x( 'Reset', 'Product overview (hover product)', 'comc' ) . '</a>';
		}

		return $actions;
	}

	/**
	 * Handle the bulk push action, simple call a function.
	 *
	 * @param $redirect_to
	 * @param $doaction
	 * @param $post_ids
	 *
	 * @return mixed
	 */
	public static function handleBulkPushToWebshops( $redirect_to, $doaction, $post_ids ) {
		if ( $doaction == self::DO_ACTION ) {
			WoocommerceProduct::preparePostDataAndSavePost( $_GET, $post_ids );
		}

		return $redirect_to;
	}

	/**
	 * @param $bulk_actions
	 *
	 * @return mixed
	 */
	public static function addBulkPushToWebshops( $bulk_actions ) {
		$bulk_actions[ self::DO_ACTION ] = __( "Push to Webshop", "comc" );

		return $bulk_actions;
	}

	/**
	 * Populate the webshops fields with a list of shared webshops (if shared).
	 *
	 * @param $column_name
	 * @param $post_id
	 */
	public static function populateWebshopsField( $column_name, $post_id ) {
		if ( $column_name == self::WEB_COL ) {
			$webshops = WoocommerceChildRelationships::getSharedWebsites( $post_id );
			if ( empty( $webshops ) ) {
				echo __( "Not shared", "comc" );
			} else {
				foreach ( $webshops as $webshopId ) {
					$webshopObj = Webshop::getWebshopById( $webshopId );
					echo "<a href='/wp-admin/post.php?post={$webshopId}&action=edit' target='_blank'> " . $webshopObj->getWebshopName() . "</a>, ";
				}
			}
		}
	}

	/**
	 * @param $column_name
	 * @param $post_type
	 */
	public static function addBulkEditWebshops( $column_name, $post_type ) {
		if ( $column_name == self::WEB_COL ) {
			self::echoWebshopsField();
		}
	}

	/**
	 * @param $postType
	 * @param $post
	 */
	public static function registerMetaBoxes( $postType, $post ) {
		if ( $postType == "product" ) {
			add_meta_box( "webshops", _x( "Share to webshop", "Product metabox", "comc" ), array(
				"WoocommerceProductPage",
				"echoWebshopsField"
			), "product", "side", "default" );
		}
	}

	/**
	 * @param $columns
	 *
	 * @return mixed
	 */
	public static function addFieldsProductTable( $columns ) {
		unset( $columns["product_tag"] );
		unset( $columns["product_type"] );
		unset( $columns["featured"] );
		$columns[ self::WEB_COL ] = __( "Webshops", "comc" );

		return $columns;
	}

	/**
	 * We provide some extra textual explanation to how to use the bulk function. We are a little bit afraid people
	 * will misuse it and remove products from webshops by accident.
	 */
	public static function echoWebshopsField() {
		global $post;
		$sharedWebshops = WoocommerceChildRelationships::getSharedWebsites( $post->ID );
		if ( $sharedWebshops == null ) {
			$sharedWebshops = array();
		}
		$webshops = Webshop::getAllWebshops();
		echo "<script>var webshops = " . json_encode( $webshops ) . "</script>";
		if ( ! isset( $_GET['action'] ) && $post->post_status != "auto-draft" ) {
			echo "<fieldset class='inline-edit-col-left'><div class='inline-edit-col'>" . __( "All products will be pushed/updated to the selected webshops and all products will be removed from the deselected webshops. Please note that when editing in bulk, the checkbox of a webshop is only checked if all the products being edited are already pushed to that specific webshop.", "comc" ) . "</div></fieldset>";
			echo "<fieldset class='inline-edit-col-center'><div class='inline-edit-col'>" . __( "When editing three products of which only two are shared to a webshop, the box is not checked! When you save the changes the product will be removed from the other two webshops. <strong>Please take caution when editing in bulk!!</strong>" ) . "</div></fieldset>";
			echo "<fieldset class='inline-edit-col-right'><div class='inline-edit-col'>";
		}
		echo "<ul class='categorychecklist'>";
		foreach ( $webshops as $webshop ) {
			$checked = "";
			if ( in_array( $webshop->ID, $sharedWebshops ) ) {
				$checked = "checked=\"checked\"";
			}
			echo "<li><label><input type='checkbox' {$checked} name='webshops[]' value=" . $webshop->ID . " />" . $webshop->post_title . "</label></li>";
		}
		echo "</ul>";
		if ( ! isset( $_GET['action'] ) && $post->post_status != "auto-draft" ) {
			echo "</div></fieldset>";
		}
	}
}
