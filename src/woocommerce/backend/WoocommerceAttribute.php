<?php
/**
 * This class is used to modify everything in relation to attributes (taxonomies).
 *
 * WooCommerce adds their own actions to add, delete and update attributes (see consturctor), we use these functions
 * as well instead of the default WordPress functions.
 */
defined( 'ABSPATH' ) OR exit;

class WoocommerceAttribute {
	const ATTR_TYPE = "attribute";
	protected static $instance;
	protected static $webshops;

	/**
	 * @return mixed
	 */
	public static function init() {
		is_null( self::$instance ) AND self::$instance == new self;
		self::$webshops = Webshop::getAllWebshops();

		return self::$instance;
	}

	/**
	 * WoocommerceAttribute constructor.
	 */
	public function __construct() {
		add_action( "woocommerce_attribute_added", array( "WoocommerceAttribute", "saveAttribute" ), 10, 2 );
		add_action( "woocommerce_attribute_deleted", array( "WoocommerceAttribute", "deleteAttribute" ), 10, 3 );
		add_action( "woocommerce_attribute_updated", array( "WoocommerceAttribute", "updateAttribute" ), 10, 3 );
	}

	/**
	 * Normally WordPress does not distinguish between adding and editting, WooCommerce does however!
	 *
	 * So when an attribute is added we receive all the information for that attribute and store it in an array.
	 * We loop over each correctly configured webshop and push the attribute to the webshop. After that for each
	 * webshop we save the relation in the child relationship table so we can later easily access all the childs.
	 *
	 * @param $insert_id
	 * @param $attribute
	 */
	public static function saveAttribute( $insert_id, $attribute ) {
		if ( $_GET["post_type"] == "product" ) {
            $exportAttribute = array(
                "name" => $_POST["attribute_label"],
                "slug" => $_POST["attribute_name"],
                "type" => $_POST["attribute_type"],
                "order_by" => $_POST["attribute_orderby"],
                "has_archives" => false
            );
            if ( ! empty( $_POST["attribute_public"] ) ) {
                if ( $_POST["attribute_public"] === "1" ) {
                    $exportAttribute["has_archives"] = true;
                }
            }
            if ( empty( $_POST["attribute_name"] ) ) {
                $exportAttribute["slug"] = sanitize_category( $_POST["attribute_label"] );
            }

        } else {
            $exportAttribute = $attribute;
        }
        foreach ( self::$webshops as $webshop ) {
            $wooApi     = new WoocommerceApi( $webshop->ID );
            $returnData = $wooApi->post( "post", "products/attributes", $exportAttribute );
            WoocommerceChildRelationships::saveChildRelationship( $insert_id, $returnData["id"], $webshop->ID, self::ATTR_TYPE );
        }
	}

	/**
	 * When an attribute is deleted we loop again over each configured webshop and delete the attributes on the child
	 * if they are shared. We also delete it from the child_relationship table.
	 *
	 * @param      $attribute_id
	 * @param null $attribute_name
	 * @param null $taxonomy
	 */
	public static function deleteAttribute( $attribute_id, $attribute_name = null, $taxonomy = null ) {
		foreach ( self::$webshops as $webshop ) {
			if ( WoocommerceChildRelationships::doesExistOnChild( $attribute_id, $webshop->ID, "attribute" ) ) {
				$child_id = WoocommerceChildRelationships::getChildRelationshipId( $attribute_id, $webshop->ID, self::ATTR_TYPE );
				WoocommerceChildRelationships::deleteChildRelationship( $attribute_id, $webshop->ID, self::ATTR_TYPE );
				$wooApi = new WoocommerceApi( $webshop->ID );
				$wooApi->post( "delete", "products/attributes/{$child_id}", null );
			}
		}
	}

	/**
	 * When an attribute is updated we simply do the same as when adding an attribute, the only difference being that we
	 * first receive the child ID and update it.
	 *
	 * Though, we had to add an extra statement to check whether the attribute was already shared, maybe people
	 * added attributes before activating our plugin. So we need to manually check this too.
	 *
	 * @param $attribute_id
	 * @param $attribute
	 * @param $old_attribute_name
	 */
	public static function updateAttribute( $attribute_id, $attribute, $old_attribute_name ) {
		if ( $_GET["post_type"] == "product" ) {
			$exportAttribute = array(
				"name"         => $_POST["attribute_label"],
				"slug"         => $_POST["attribute_name"],
				"type"         => $_POST["attribute_type"],
				"order_by"     => $_POST["attribute_orderby"],
				"has_archives" => false
			);
			if ( isset( $_POST["attribute_public"] ) && $_POST["attribute_public"] === "1" ) {
				$exportAttribute["has_archives"] = true;
			}
			foreach ( self::$webshops as $webshop ) {
				$wooApi = new WoocommerceApi( $webshop->ID );
				if ( WoocommerceChildRelationships::doesExistOnChild( $attribute_id, $webshop->ID, "attribute" ) ) {
					$child_id = WoocommerceChildRelationships::getChildRelationshipId( $attribute_id, $webshop->ID, self::ATTR_TYPE );
					$wooApi->post( "put", "products/attributes/{$child_id}", $exportAttribute );
				} else {
					$returnData = $wooApi->post( "post", "products/attributes", $exportAttribute );
					WoocommerceChildRelationships::saveChildRelationship( $attribute_id, $returnData["id"], $webshop->ID, self::ATTR_TYPE );
				}

			}
		}
	}
}