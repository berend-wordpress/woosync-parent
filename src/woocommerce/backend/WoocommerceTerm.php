<?php
/**
 * This class is used to process anything related to adding changing and deleting terms.
 *
 * Important to know is that tags are terms too, WordPress does not distinguish between these so they all fire the
 * hooks below. Same for the categories. Sadly the WooCommerce API does distinguish between these different kind
 * of terms. So we have to manually check if we are working with tags, attribute terms, of categories.
 *
 * Other than that woocommerce has its own tanle to save extra attributes, nice and all but these attributes
 * are connected to terms by name, not ID? You can se "pa" for each attribute to show its a product
 * attribute. However for each term we need to provide the taxonomy, so we have to manually receive the taxonomy ID
 * from the child. HOWEVER - that is not the taxonomy ID that WordPress saves but the WooCcmmerce attribute ID in the
 * manually added table from WooCommerce. And this ID is obviously not provided in the hooks used below.
 *
 * So, we have to manually receive all this information everytime. I will describe this more proplery in the
 * technical documentation.
 *
 * Deze class word gebruikt om de acties voor het aanmaken, verwijderen en wijzigen van terms aan te maken.
 */
defined( 'ABSPATH' ) OR exit;

class WoocommerceTerm {
	const TERM_TYPE = "term";
	protected static $instance;
	protected static $webshops;

	/**
	 * @return mixed
	 */
	public static function init() {
		is_null( self::$instance ) AND self::$instance == new self;
		self::$webshops = Webshop::getAllWebshops();

		return self::$instance;
	}

	/**
	 * WoocommerceTerm constructor.
	 */
	public function __construct() {
		add_action( "create_term", array( "WoocommerceTerm", "saveTerm" ), 10, 3 );
		add_action( "edit_term", array( "WoocommerceTerm", "updateTerm" ), 10, 5 );
		add_action( "delete_term", array( "WoocommerceTerm", "deleteTerm" ), 10, 3 );
	}

	/**
	 * As said earlier, we have to do different things when working with tags, categories or terms.
	 *
	 * @param $term_id
	 * @param $tt_id
	 * @param $taxonomy
	 */
	public static function saveTerm( $term_id, $tt_id, $taxonomy ) {
		if ( $_POST["post_type"] == "product" || $_POST["action"] == "add-product_cat" || $_POST['post_type'] == 'webshop' ) {
			foreach ( self::$webshops as $webshop ) {
				if ( isset( $_POST["tag-name"] ) ) {
					$exportTerm["name"] = $_POST["tag-name"];
				} else {
					$localTerm          = get_term( $term_id );
					$exportTerm["name"] = $localTerm->name;
				}
				$wooApi = new WoocommerceApi( $webshop->ID );
				/**
				 * Product Tags
				 */
				if ( $taxonomy == "product_tag" ) {
					$exportTerm["name"] = Utility::getTermNameById( $term_id );
					$returnData         = $wooApi->post( "post", "products/tags", $exportTerm );
				} /**
				 * Product Category
				 */ elseif ( $taxonomy == "product_cat" ) {
					if ( isset( $_POST["parent"] ) && $_POST["parent"] != "-1" ) {
						$parentCatChildId     = WoocommerceChildRelationships::getChildRelationshipId( $_POST["parent"], $webshop->ID, self::TERM_TYPE );
						$exportTerm["parent"] = (int) $parentCatChildId;
					}
					if ( $_POST["description"] != "" ) {
						$exportTerm["description"] = $_POST["description"];
					}
					if ( $_POST["display_type"] != "" ) {
						$exportTerm["display"] = $_POST["display_type"];
					}
					if ( ! empty( $_POST["product_cat_thumbnail_id"] ) && $_POST["product_cat_thumbnail_id"] != "-1" ) {
						$url                 = wp_get_attachment_image_src( $_POST["product_cat_thumbnail_id"], "full" );
						$exportTerm["image"] = array( "src" => $url[0] );
					}
					$returnData = $wooApi->post( "post", "products/categories", $exportTerm );

				}
				/**
				 * Other Product terms
				 */ else {
					$wooAttrId      = Utility::getWooAttrIdByName( $taxonomy );
					$wooAttrChildId = WoocommerceChildRelationships::getChildRelationshipId( $wooAttrId, $webshop->ID, WoocommerceAttribute::ATTR_TYPE );
					$returnData     = $wooApi->post( "post", "products/attributes/{$wooAttrChildId}/terms", $exportTerm );
				}
				/**
				 * Save relationship
				 */
				if ( ! WoocommerceChildRelationships::saveChildRelationship( $term_id, $returnData["id"], $webshop->ID, self::TERM_TYPE, 0 ) ) {
					wp_die( __( "Can't create the child realtionship", "comc" ) );
				}
			}
		}
	}

	/**
	 * @param $term
	 * @param $tt_id
	 * @param $taxonomy
	 * @param $deleted_term
	 * @param $object_ids
	 */
	public static function deleteTerm( $term, $tt_id, $taxonomy, $deleted_term, $object_ids ) {
		foreach ( self::$webshops as $webshop ) {
			$wooApi = new WoocommerceApi( $webshop->ID );
			/**
			 * Product Tags
			 */
			if ( $taxonomy == "product_tag" ) {
				$wooTermChildId = WoocommerceChildRelationships::getChildRelationshipId( $term, $webshop->ID, self::TERM_TYPE );
				$wooApi->post( "delete", "products/tags/{$wooTermChildId}", null );

			} /**
			 * Product Category
			 */ elseif ( $taxonomy == "product_cat" ) {
				$wooCatChildId = WoocommerceChildRelationships::getChildRelationshipId( $term, $webshop->ID, self::TERM_TYPE );
				$wooApi->post( "delete", "products/categories/{$wooCatChildId}", null );
			} /**
			 * Other Product tags
			 */ else {
				$wooAttrId      = Utility::getWooAttrIdByName( $taxonomy );
				$wooAttrChildId = WoocommerceChildRelationships::getChildRelationshipId( $wooAttrId, $webshop->ID, WoocommerceAttribute::ATTR_TYPE );
				$wooTermChildId = WoocommerceChildRelationships::getChildRelationshipId( $term, $webshop->ID, self::TERM_TYPE );
				$wooApi->post( "delete", "products/attributes/{$wooAttrChildId}/terms/{$wooTermChildId}", null );
			}
			/*
			 * Delete child relationship
			 */
			if ( ! WoocommerceChildRelationships::deleteChildRelationship( $term, $webshop->ID, self::TERM_TYPE ) ) {
				wp_die( __( "Can't remove the term connection", "comc" ) );
			}
		}
	}

	/**
	 * @param $term_id
	 * @param $taxonomy
	 */
	public static function updateTerm( $term_id, $taxonomy ) {
		$exportTerm["name"]        = $_POST["name"];
		$exportTerm["slug"]        = $_POST["slug"];
		$exportTerm["description"] = $_POST["description"];
		foreach ( self::$webshops as $webshop ) {
			$childTermId = WoocommerceChildRelationships::getChildRelationshipId( $term_id, $webshop->ID, self::TERM_TYPE );
			$wooApi      = new WoocommerceApi( $webshop->ID );
			/**
			 * Product Tags
			 */
			if ( Utility::getTaxonomyNameById( $taxonomy ) == "product_tag" ) {
				$wooApi->post( "put", "products/tags/{$childTermId}", $exportTerm );
			} /**
			 * Product Category
			 */ elseif ( Utility::getTaxonomyNameById( $taxonomy ) == "product_cat" ) {
				if ( $_POST["parent"] != "-1" ) {
					$parentCatChildId     = WoocommerceChildRelationships::getChildRelationshipId( $_POST["parent"], 0, self::TERM_TYPE );
					$exportTerm["parent"] = (int) $parentCatChildId;
				} else {
					$exportTerm["parent"] = 0;
				}
				if ( ! empty( $_POST["product_cat_thumbnail_id"] ) && $_POST["_thumbnail_id"] != "-1" ) {
					$url                  = wp_get_attachment_image_src( $_POST["product_cat_thumbnail_id"], "full" );
					$exportTerm["images"] = array( array( "src" => $url[0] ) );
				}
				if ( $_POST["display_type"] != "" ) {
					$exportTerm["display"] = $_POST["display_type"];
				}
				$wooApi->post( "put", "products/categories/{$childTermId}", $exportTerm );

			} /**
			 * Other Product terms
			 */ else {
				$wooAttrChildId = Utility::getChildAtrributeIdByParentTaxonomyId( $taxonomy, WoocommerceAttribute::ATTR_TYPE, $webshop->ID );
				$wooTermChildId = WoocommerceChildRelationships::getChildRelationshipId( $term_id, $webshop->ID, self::TERM_TYPE );
				$wooApi->post( "put", "products/attributes/{$wooAttrChildId}/terms/{$wooTermChildId}", $exportTerm );
			}
		}
	}
}