<?php
/**
 * This class is used to modify anything related to the Products in WooCommerce. Its quite a huge class and we tried to
 * seperate different parts in different functions to keep it readable. Its mostly about the saveProduct function
 * which actually saves and shares the products to the child websites. It also deleted the products from webshops if
 * webshops are deselected in the edit product screen.
 *
 * In the save product function we call different functions to process various settings such as:
 *
 * - Images
 * - Attributes
 * - Tags
 * - Categories
 * - Variations
 * - Downloadable files
 *
 * Each function can be find almost at the bottom of the class. We also call a utility function to check whether we need
 * to read from $_POST or $_GET. (Depends on whether or not bulk functions are used).
 *
 * We have two more functions: preparePostDateAndSavePost and preparePostDataVariations. These functions were added
 * later to be able to properly use bulk functions and call the savePost function from more locations than just
 * the default WordPress action.
 *
 * Further explanation can be found at each function in the class.
 *
 */
defined( 'ABSPATH' ) OR exit;

class WoocommerceProduct {
	const PRODUCT_TYPE = "product";
	protected static $instance;
	protected static $webshops;

	public static function init() {
		is_null( self::$instance ) AND self::$instance == new self;
		self::$webshops = Webshop::getAllWebshops();

		return self::$instance;
	}

	/**
	 * With the save post action we can tell the action only to fire when the post type is "Product" sadly, we can
	 * not do this with trash, untrash and delete. So we check this in each of these 3 functions manually.
	 * WoocommerceProduct constructor.
	 */
	public function __construct() {
		add_action( "save_post_product", array( "WoocommerceProduct", "saveProduct" ) );
		add_action( "wp_trash_post", array( "WoocommerceProduct", "trashProduct" ) );
		add_action( "before_delete_post", array( "WoocommerceProduct", "deleteProduct" ), 10 );
		add_action( "untrash_post", array( "WoocommerceProduct", "untrashProduct" ), 10 );
	}

	/**
	 * This function is ment to receive all the information about a product and its variations.
	 * In multiple locations the data we receive is very limited (reset post, push in bulk) so this function
	 * populates the postData array with all the information and sends it to the saveProduct function.
	 *
	 * It is not possible to call the saveProduct function directly, this only works from the edit post screen.
	 *
	 * @param $getData
	 * @param $product_ids
	 */
	public static function preparePostDataAndSavePost( $getData, $product_ids ) {
		$webshopId = (int) $getData["webshop"];
		foreach ( $product_ids as $product_id ) {
			$sharedWebshops   = WoocommerceChildRelationships::getSharedWebsites( $product_id );
			$sharedWebshops[] = $webshopId;
			$product          = wc_get_product( $product_id );
			$productData      = $product->get_data();
			$imageIds         = implode( $productData["gallery_image_ids"], "," );
			$wcFileNames      = null;
			$wcFileUrls       = null;
			$i                = 0;
			/**
			 * Prepare downloadable files
			 */
			foreach ( $productData["downloads"] as $download ) {
				/** @var $download WC_Product_Download */
				$downloadData      = $download->get_data();
				$wcFileNames[ $i ] = $downloadData["name"];
				$wcFileUrls[ $i ]  = $downloadData["file"];
				$i ++;
			}
			/**
			 * Prepare attributes
			 */
			$attributeNames     = null;
			$attributeValues    = null;
			$attributeVariation = null;
			$i                  = 0;
			foreach ( $productData["attributes"] as $attribute ) {
				/** @var $attribute WC_Product_Attribute */
				$attributeData         = $attribute->get_data();
				$attributeNames[ $i ]  = $attributeData["name"];
				$attributeValues[ $i ] = $attributeData["options"];
				if ( $attributeData["is_variation"] == 1 ) {
					$attributeVariation[ $i ] = "1";
				}
				$i ++;
			}
			/**
			 * Prepare all the parent product attributes
			 */
			$postData = array(
				"webshops"              => $sharedWebshops,
				"action"                => "editpost",
				"name"                  => $productData["name"],
				"_regular_price"        => $productData["regular_price"],
				"_sale_price"           => $productData["sale_price"],
				"product-type"          => "simple",
				"_sku"                  => $productData["sku"],
				"_wight"                => $productData["weight"],
				"_height"               => $productData["height"],
				"_width"                => $productData["width"],
				"_length"               => $productData["length"],
				"_downloadable"         => $productData["downloadable"],
				"_thumbnail_id"         => $productData["image_id"],
				"product_image_gallery" => $imageIds,
				"tax_input"             => array(
					"product_cat" => $productData["category_ids"],
					"product_tag" => $productData["tag_ids"]
				),
				"_wc_file_names"        => $wcFileNames,
				"_wc_file_urls"         => $wcFileUrls,
				"attribute_values"      => $attributeValues,
				"attribute_names"       => $attributeNames,
				"attribute_variation"   => $attributeVariation
			);
			/**
			 * If the products has childs (WooCommerce function) we need to prepare and post the variations too.
			 */
			if ( $product->has_child() ) {
				$postData["product-type"] = "variable";
				self::preparePostDataVariations( $postData, $product );
			}
			self::saveProduct( $product_id, $postData );
		}
	}

	/**
	 * When pushing products in bulk the variation data is missing, also when pushing a product from the edit product
	 * page the variations are not send in the postdata when the tab is not viewed... This is pretty cumbersome because
	 * we have two situations now in which we need to receive the product variations manually.
	 *
	 * This functions read the variations from the product and prepares the postData array with the correct information
	 * so it can be passed to the saveProduct function.
	 *
	 * Function is called in bulkPushToWebshop because variations are not send with bulk actions.
	 * Function is called in saveProducts only if the product is variable, but variations are missing.
	 *
	 * @param $postData
	 * @param $product
	 */
	public static function preparePostDataVariations( &$postData, $product ) {
		/** @var $product WC_Product */
		$productVariations = $product->get_children();
		if ( $productVariations != null ) {
			$i = 0;
			foreach ( $productVariations as $productVariationId ) {
				$productVariation                           = wc_get_product( $productVariationId );
				$productVarData                             = $productVariation->get_data();
				$postData["variable_post_id"][ $i ]         = $productVariationId;
				$postData["variable_regular_price"][ $i ]   = $productVarData["regular_price"];
				$postData["variable_sku"][ $i ]             = $productVarData["sku"];
				$postData["variable_sale_price"][ $i ]      = $productVarData["sale_price"];
				$postData["variable_stock_status"][ $i ]    = $productVarData["stock_status"];
				$postData["variable_weight"][ $i ]          = $productVarData["weight"];
				$postData["variable_length"][ $i ]          = $productVarData["length"];
				$postData["variable_width"][ $i ]           = $productVarData["width"];
				$postData["variable_height"][ $i ]          = $productVarData["height"];
				$postData["variable_description"][ $i ]     = $productVarData["description"];
				$postData["variable_download_limit"][ $i ]  = $productVarData["download_limit"];
				$postData["variable_download_expiry"][ $i ] = $productVarData["download_expiry"];
				$postData["variable_enabled"][ $i ]         = "on";
				$i ++;
			}
		}
	}

	/**
	 * 1. This function is triggered when a new post is saved in WordPress, we added it only to posts of type "Product".
	 * We check manually if the post_status is published, we will never push products to child websites if the
	 * status is not published.
	 *
	 * 2. Save post means creating a new post as well as changing an already existing posts, we have to distinguish
	 * between these two actions and post/put the data accordingly.
	 *
	 * 3. When we use the bulk edit, we receive our data in a GET request, though all other actions sends the data though
	 * a POST request. So we validate first where we need to get our data and add it to the $postData array instead
	 * of working from the $_POST or $_GET arrays directly.
	 *
	 * 4. See more comments inside the function for further explanation.
	 *
	 * @param $id
	 * @param $postData
	 */
	public static function saveProduct( $id, $postData = null ) {
		$post = get_post( $id );
		if ( $post->post_status == "publish" ) {
			if ( $postData == null ) {
				$postData = Utility::validateRequest( $_POST, $_GET );
			}
			/**
			 * This code was later added so give users the ability to share entire categories. What this part does
			 * is simply read out to which categories the product was added (or already in) and check if this category
			 * is shared entirely to a webshop. If so, we pretend that the checkbox for this webshop is checked in the
			 * edit product screen, thus pushing the product to that specific webshop. This is properly described in the
			 * documentation for the users, so they should know about it.
			 */
			if ( isset( $postData["tax_input"]["product_cat"] ) ) {
				$sharedCats = $postData["tax_input"]["product_cat"];
				foreach ( $sharedCats as $catId ) {
					if ( $catId == "0" ) {
						continue;
					}
					$webshops = get_posts( array(
						"post_type" => "Webshop",
						"tax_query" => array(
							array(
								"taxonomy" => "product_cat",
								"field"    => "ID",
								"terms"    => $catId
							)
						)
					) );
					foreach ( $webshops as $webshop ) {
						if ( ! WoocommerceChildRelationships::doesExistOnChild( $id, $webshop->ID ) ) {
							$postData["webshops"][] = $webshop->ID;
						}
					}
				}
			}
			/**
			 * Continue with looping over each webshop, check first if the webshop was selected. Than continue..
			 */
			foreach ( self::$webshops as $webshop ) {
				$wooApi = new WoocommerceApi( $webshop->ID );
				if ( in_array( $webshop->ID, $postData["webshops"] ) ) {
					/**
					 * Some basic product info.
					 *
					 * We will store everything in the $exportProduct array, this array will eventuall be pushed
					 * to the child.
					 */
					$product       = wc_get_product( $post->ID );
					$productData   = $product->get_data();
					$exportProduct = array(
						'name'              => $productData["name"],
						'type'              => $postData["product-type"],
						'regular_price'     => $postData["_regular_price"],
						'sale_price'        => $postData["_sale_price"],
						'sku'               => $postData["_sku"],
						'description'       => $productData["description"],
						'short_description' => $productData["short_description"],
						'weight'            => $postData["_weight"],
						"dimensions"        => array(
							'height' => $postData["_height"],
							'length' => $postData["_length"],
							'width'  => $postData["_width"],
						),
					);
					if ( $postData["_manage_stock"] == "yes" ) {
						$exportProduct["manage_stock"] = true;
					} else {
						$exportProduct["manage_stock"] = false;
					}
					if ( isset( $postData["_stock"] ) ) {
						$exportProduct["stock_quantity"] = $postData["_stock"];
					}
					if ( $postData["_stock_status"] == "instock" ) {
						$exportProduct["in_stock"] = true;
					} else {
						$exportProduct["in_stock"] = false;
					}
					if ( $postData["_backorders"] == "no" ) {
						$exportProduct["backorders"] = "no";
					} else if ( $postData["_backorders"] == "notify" ) {
						$exportProduct["backorders"] = "notify";
					} else {
						$exportProduct["backorders"] = "yes";
					}
					if ( isset( $postData["_sold_individually"] ) && $postData["_sold_individually"] == "yes" ) {
						$exportProduct["sold_individually"] = true;
					}
					/**
					 * Apperently the WooCommerce Api expects the price to be a string.
					 */
					if ( $webshop->prijsMarge > 0 ) {
						if ( $postData["_regular_price"] > 0 ) {
							$exportProduct["regular_price"] = (String) Utility::calculatePriceMarge( $postData["_regular_price"], $webshop->prijsMarge );
						}
						if ( $postData["_sale_price"] > 0 ) {
							$exportProduct["sale_price"] = (String) Utility::calculatePriceMarge( $postData["_sale_price"], $webshop->prijsMarge );
						}
					}
					/**
					 * Validate different product options and process them to the exportProduct array.
					 *
					 * These actions are put in their own functions to make the saveProduct function more readable. The
					 * actual functions can be found at the bottom of this class.
					 */
					if ( ! empty( $postData["_downloadable"] ) && $postData["_downloadable"] == "on" ) {
						self::processDownloadableFiles( $postData, $exportProduct );
					}
					if ( ! empty( $postData["_thumbnail_id"] ) && $postData["_thumbnail_id"] != "-1" ) {
						self::processThumbnail( $postData, $exportProduct );
					}
					if ( ! empty( $postData["product_image_gallery"] ) ) {
						self::processGallery( $postData, $exportProduct );
					}
					if ( ! empty( $postData["tax_input"]["product_cat"] ) ) {
						self::processCategories( $postData, $exportProduct, $webshop );
					}
					if ( ! empty( $postData["tax_input"]["product_tag"] ) ) {
						self::processTags( $postData, $exportProduct, $webshop );
					}
					if ( ! empty( $postData["attribute_values"] ) && ! empty( $postData["attribute_names"] ) ) {
						self::processAttributes( $postData, $exportProduct, $webshop );

					}
					/**
					 * Now we distinguish between a "new" product and editting an already existing product.
					 * If an existing product is changed and shared to the webshop we check if the product is already
					 * shared or not. If it is not shared yet, we will do a post requests to add it. If it is already
					 * shared before, we execute a put request to update the product instead.
					 *
					 * If a new product is added we can simply post it to the child webshop.
					 *
					 * This part can be improved, we do not used the doesExistOnChild function at all, and I see the
					 * same if statement multiple times.
					 *
					 * @TODO Improve code
					 **/
					$productChildId = WoocommerceChildRelationships::getChildRelationshipId( $id, $webshop->ID, self::PRODUCT_TYPE );
					if ( empty( $postData["bulk_edit"] ) ) {
						$postData["bulk_edit"] = "";
					}
					if ( ( isset( $postData["save"] ) && $postData["save"] == "Update" ) || $postData["bulk_edit"] == "Update" ) {
						if ( $productChildId != null ) {
							$wooApi->post( "put", "products/{$productChildId}", $exportProduct, $id );
						} else {
							$returnData = $wooApi->post( "post", "products", $exportProduct );
							WoocommerceChildRelationships::saveChildRelationship( $post->ID, $returnData["id"], $webshop->ID, self::PRODUCT_TYPE );
						}
					} else {
						if ( $productChildId == null ) {
							$returnData = $wooApi->post( "post", "products", $exportProduct );
							WoocommerceChildRelationships::saveChildRelationship( $post->ID, $returnData["id"], $webshop->ID, self::PRODUCT_TYPE );
						} else {
							$wooApi->post( "put", "products/{$productChildId}", $exportProduct, $id );
						}
					}
					/**
					 * After the product is pushed we check if the product is variable, if so we have to update the
					 * product variations after the parent product has been pushed to the child webshop since we need
					 * the child (webshop) parent (product) ID when pushing variations to the child webshop.
					 */
					if ( $postData["product-type"] == "variable" ) {
						if ( ! isset( $postData["variable_post_id"] ) ) {
							self::preparePostDataVariations( $postData, $product );
						}
						self::processVariations( $postData, $webshop, $post, $wooApi );
					}

				} /**
				 * If the product is not shared to a webshop but it has been shared before we will delete it.
				 *
				 * When untrashing a post (republish) this function is also executd because it is seen as saving a post.
				 * We do not want to delete the product if it is untrashed so we have a manual check here.
				 *
				 */ else {
					if ( $postData["action"] != "untrash" ) // Manual check against untrash
					{
						$productChildId = WoocommerceChildRelationships::getChildRelationshipId( $id, $webshop->ID, self::PRODUCT_TYPE );
						if ( $productChildId != null ) {
							$wooApi->post( "delete", "products/{$productChildId}", null );
							if ( isset( $postData["tax_input"]["product_cat"] ) ) {
								$sharedCats = $postData["tax_input"]["product_cat"];
								foreach ( $sharedCats as $catId ) {
									if ( $catId == "0" ) {
										continue;
									}
									if ( has_term( $catId, "product_cat", $webshop->ID ) ) {
										wp_remove_object_terms( $id, (int) $catId, "product_cat" );
									}
								}
							}
							self::deleteAllVariations( $id, $webshop->ID );
							WoocommerceFieldChanges::resetProduct( $webshop->ID, $id );
							WoocommerceChildRelationships::deleteChildRelationship( $id, $webshop->ID, self::PRODUCT_TYPE );
						}
					}
				}
			}
		}
	}

	/**
	 * Becuase the key and values passed to the woocommerce api are "sometimes" different from the key and values recieved
	 * from the save post page we need to check these options manually.
	 *
	 * In the beginning of the function we recieve all product variations saved for this product ID before the edit,
	 * we also receive all the variations after the update ($postData["variable_postId"]).
	 *
	 * So when we loop over all the new product variations (maybe more, maybe less then before) we can update each existing variation
	 * if it is changed, and insert a new one if one is added. When the variation is handled we remove it from the
	 * localVarIds array. After the foreach is done we are left with either an empty localVarIds array (if nothing needs
	 * to be deleted) or an array with variation products ID's which need to be deleted from the child website.
	 *
	 * @param                $postData
	 * @param                $webshop
	 * @param                $post
	 * @param WoocommerceApi $wooApi
	 */
	public static function processVariations( $postData, $webshop, $post, $wooApi ) {
		$localVarIds = WoocommerceChildRelationships::getVariationsByParentId( $post->ID, $webshop->ID );
		foreach ( $postData["variable_post_id"] as $varIndex => $varId ) {
			unset( $localVarIds[ $varId ] );
			$varProduct       = wc_get_product( $varId );
			$varProductData   = $varProduct->get_data();
			$varExportProduct = null;
			foreach ( $varProductData["attributes"] as $attribute => $term ) {
				$parentAttrId                                = Utility::getWooAttrIdByName( $attribute );
				$childAttrId                                 = WoocommerceChildRelationships::getChildRelationshipId( $parentAttrId, $webshop->ID, WoocommerceAttribute::ATTR_TYPE );
				$varExportProduct["attributes"][ $varIndex ] = array( "id" => $childAttrId, "option" => $term );
				/**
				 * Alle eigenschappen van de variable producten alleen overnemen als deze ook veranderd zijn.
				 */
				if ( $postData["variable_regular_price"][ $varIndex ] != "" ) {
					$varExportProduct["regular_price"] = $postData["variable_regular_price"][ $varIndex ];
				}
				if ( $postData["variable_sku"][ $varIndex ] != "" ) {
					$varExportProduct["sku"] = $postData["variable_sku"][ $varIndex ];
				}
				if ( $postData["variable_sale_price"][ $varIndex ] != "" ) {
					$varExportProduct["sale_price"] = $postData["variable_sale_price"][ $varIndex ];
				}
				if ( $postData["variable_stock_status"][ $varIndex ] == "instock" ) {
					$varExportProduct["in_stock"] = true;
				} else {
					$varExportProduct["in_stock"] = false;
				}
				if ( $postData["variable_weight"][ $varIndex ] != "" ) {
					$varExportProduct["weight"] = $postData["variable_weight"][ $varIndex ];
				}
				if ( $postData["variable_length"][ $varIndex ] != "" ) {
					$varExportProduct["dimensions"]["length"] = $postData["variable_length"][ $varIndex ];
				}
				if ( $postData["variable_width"][ $varIndex ] != "" ) {
					$varExportProduct["dimensions"]["width"] = $postData["variable_width"][ $varIndex ];
				}
				if ( $postData["variable_height"][ $varIndex ] != "" ) {
					$varExportProduct["dimensions"]["height"] = $postData["variable_height"][ $varIndex ];
				}
				if ( $postData["variable_description"][ $varIndex ] != "" ) {
					$varExportProduct["description"] = $postData["variable_description"][ $varIndex ];
				}
				if ( $postData["variable_download_limit"][ $varIndex ] != "" ) {
					$varExportProduct["download_limit"] = $postData["variable_download_limit"][ $varIndex ];
				}
				if ( $postData["variable_download_expiry"][ $varIndex ] != "" ) {
					$varExportProduct["download_expiry"] = $postData["variable_download_expiry"][ $varIndex ];
				}

			}
			$childProductId = WoocommerceChildRelationships::getChildRelationshipId( $post->ID, $webshop->ID, self::PRODUCT_TYPE );
			/**
			 * Check if the variation already exists,if it does we update with a put request, if not we create it
			 * with a post request.
			 */
			$childVarId = WoocommerceChildRelationships::getChildRelationshipId( $varId, $webshop->ID, self::PRODUCT_TYPE );
			if ( $childVarId == null ) {
				$returnData = $wooApi->post( "post", "products/{$childProductId}/variations", $varExportProduct );
				WoocommerceChildRelationships::saveChildRelationship( $varId, $returnData["id"], $webshop->ID, self::PRODUCT_TYPE, $post->ID );
			} else {
				$wooApi->post( "put", "products/{$childProductId}/variations/{$childVarId}", $varExportProduct, $post->ID );
			}
		}
		if ( ! empty( $localVarIds ) ) {
			foreach ( $localVarIds as $varId ) {
				$childVarId     = WoocommerceChildRelationships::getChildRelationshipId( $varId, $webshop->ID, self::PRODUCT_TYPE );
				$childProductId = WoocommerceChildRelationships::getChildRelationshipId( $post->ID, $webshop->ID, self::PRODUCT_TYPE );
				$wooApi->post( "delete", "products/{$childProductId}/variations/{$childVarId}", null );
				WoocommerceChildRelationships::deleteChildRelationship( $varId, $webshop->ID, self::PRODUCT_TYPE );
			}
		}
	}

	/**
	 * Below you can find the process functions as described earlier in the class. These functions modify the $postData
	 * array directly and are accessible again in the savePost function where these functions are called.
	 *
	 */
	/**
	 * @param $postData
	 * @param $exportProduct
	 */
	public static function processDownloadableFiles( $postData, &$exportProduct ) {
		$exportProduct["downloadable"] = true;
		$filenames                     = $postData["_wc_file_names"];
		$files                         = $postData["_wc_file_urls"];
		foreach ( $files as $fileId => $fileUrl ) {
			$exportProduct["downloads"][] = array( 'name' => $filenames[ $fileId ], 'file' => $fileUrl );
		}
	}

	/**
	 * The position is important for images, when you send an image to position 0 it will be used as thumbnail.
	 *
	 * @param $postData
	 * @param $exportProduct
	 */
	public static function processThumbnail( $postData, &$exportProduct ) {
		$imgUrl                  = wp_get_attachment_image_src( $postData["_thumbnail_id"], "full" );
		$exportProduct["images"] = array( array( "src" => $imgUrl[0], "position" => 0, ) );
	}

	/**
	 * Here again is the position of high importance, we start at position 1 since we do not want to overwrite the
	 * current thumbnail (position 0)
	 *
	 * @param $postData
	 * @param $exportProduct
	 */
	public static function processGallery( $postData, &$exportProduct ) {
		$imageIds = explode( ",", $postData["product_image_gallery"] );
		$pos      = 1;
		foreach ( $imageIds as $imgId ) {
			$imgUrl                    = wp_get_attachment_image_src( $imgId, "full" );
			$exportProduct["images"][] = array( "src" => $imgUrl[0], "position" => $pos );
			$pos ++;
		}
	}

	/**
	 * @param $postData
	 * @param $exportProduct
	 * @param $webshop
	 */
	public static function processCategories( $postData, &$exportProduct, $webshop ) {
		foreach ( $postData["tax_input"]["product_cat"] as $index => $catId ) {
			if ( $catId == "0" ) {
				continue;
			}
			$childCatId = WoocommerceChildRelationships::getChildRelationshipId( $catId, $webshop->ID, WoocommerceTerm::TERM_TYPE );
			if ( $childCatId == null ) {
				WoocommerceTerm::saveTerm( $catId, $catId, "product_cat" );
				$childCatId = WoocommerceChildRelationships::getChildRelationshipId( $catId, $webshop->ID, WoocommerceTerm::TERM_TYPE );

			}
			$exportProduct["categories"][] = array( "id" => $childCatId );
		}
		if ( ! isset( $exportProduct["categories"] ) ) {
			$exportProduct["categories"][] = array( "id" => "0" );
		}
	}

	/**
	 * Tags ID's are given when a tag already exist, when a new tag is created with the product we receive
	 * only the tag name. The tag is already created though, so we can receive the ID manually.
	 *
	 * @param $postData
	 * @param $exportProduct
	 * @param $webshop
	 */
	public static function processTags( $postData, &$exportProduct, $webshop ) {
		foreach ( $postData["tax_input"]["product_tag"] as $index => $mixedTag ) {
			if ( ! is_numeric( $mixedTag ) ) {
				$tagId = Utility::getTermIdByName( $mixedTag );

			} else {
				$tagId = $mixedTag;
			}
			$childTagId = WoocommerceChildRelationships::getChildRelationshipId( $tagId, $webshop->ID, WoocommerceTerm::TERM_TYPE );
			if ( $childTagId != null ) {
				$exportProduct["tags"][] = array( "id" => $childTagId );
			}
		}
	}

	/**
	 * It is possible for an attribute to have 1 or more values (tags). When only 1 tag is selected we receive it as
	 * string. When multiple tags are selected we receive an array.
	 *
	 * @param $postData
	 * @param $exportProduct
	 * @param $webshop
	 */
	public static function processAttributes( $postData, &$exportProduct, $webshop ) {
		foreach ( $postData["attribute_names"] as $index => $name ) {
			$wooAttrId     = Utility::getWooAttrIdByName( $name );
			$childAttrId   = WoocommerceChildRelationships::getChildRelationshipId( $wooAttrId, $webshop->ID, WoocommerceAttribute::ATTR_TYPE );
			if ($childAttrId === null && $wooAttrId !== null) {
                WoocommerceAttribute::saveAttribute($wooAttrId, wc_get_attribute($wooAttrId));
                WoocommerceChildRelationships::getChildRelationshipId( $wooAttrId, $webshop->ID, WoocommerceAttribute::ATTR_TYPE );
            }

			$exportOptions = null;
			if ( is_array( $postData["attribute_values"][ $index ] ) ) {
				foreach ( $postData["attribute_values"][ $index ] as $option ) {
					$exportOptions[] = Utility::getTermNameById( (int) $option );
				}
			} else {
				$exportOptions[] = $postData["attribute_values"][ $index ];
			}
			$attributes = array(
                "name"    => $name,
                "slug"    => sanitize_title( $name ),
                "visible" => true,
                "options" => $exportOptions
            );
			if ($childAttrId !== null) {
			   $attributes['id'] = $childAttrId;
            }

			$exportProduct["attributes"][] = $attributes;

			if ( ! empty( $postData["attribute_variation"] ) ) {
				if ( $postData["attribute_variation"][ $index ] == "1" ) {
					$exportProduct["attributes"][ $index ]["variation"] = true;
				}
			}
			$exportOptions = null;
		}
	}

	/**
	 * Delete all the variations from a specific webshop bu the parent id (product id on the main site).
	 *
	 * @param $parent_id
	 * @param $webshop_id
	 */
	public static function deleteAllVariations( $parent_id, $webshop_id ) {
		$productVariations = WoocommerceChildRelationships::getVariationsByParentId( $parent_id, $webshop_id );
		foreach ( $productVariations as $productVariation ) {
			WoocommerceChildRelationships::deleteChildRelationship( $productVariation, $webshop_id, "product" );

		}
	}

	/**
	 * When a product is trashed we need to trash it on each child webshop too.
	 *
	 * @param $id
	 */
	public static function trashProduct( $id ) {
		$post           = get_post( $id );
		$sharedWebshops = WoocommerceChildRelationships::getSharedWebsites( $id );
		if ( $post->post_type == "product" ) {
			foreach ( self::$webshops as $webshop ) {
				if ( in_array( $webshop->ID, $sharedWebshops ) ) {
					$productChildId = WoocommerceChildRelationships::getChildRelationshipId( $id, $webshop->ID, self::PRODUCT_TYPE );
					$wooApi         = new WoocommerceApi( $webshop->ID );
					$wooApi->post( "trash", "products/{$productChildId}", null );
				}
			}
		}
	}

	/**
	 * When a product is untrahsed we need to untrash it on each child webshop too.
	 *
	 * @param $id
	 */
	public static function untrashProduct( $id ) {
		$post = get_post( $id );
		if ( $post->post_type == "product" ) {
			$sharedWebshops = WoocommerceChildRelationships::getSharedWebsites( $id );
			foreach ( self::$webshops as $webshop ) {
				if ( in_array( $webshop->ID, $sharedWebshops ) ) {
					$productChildId = WoocommerceChildRelationships::getChildRelationshipId( $id, $webshop->ID, self::PRODUCT_TYPE );
					$wooApi         = new WoocommerceApi( $webshop->ID );
					$wooApi->post( "put", "products/{$productChildId}", array( "status" => "publish" ) );
				}
			}
		}
	}

	/**
	 * When a product is deleted we permanently delete it from the child as well. We also remove the relationship from
	 * the child_relationship table.
	 *
	 * @param $id
	 */
	public static function deleteProduct( $id ) {
		$post = get_post( $id );
		if ( $post->post_type == "product" ) {
			$sharedWebshops = WoocommerceChildRelationships::getSharedWebsites( $id );
			foreach ( self::$webshops as $webshop ) {
				if ( in_array( $webshop->ID, $sharedWebshops ) ) {
					$productChildId = WoocommerceChildRelationships::getChildRelationshipId( $id, $webshop->ID, self::PRODUCT_TYPE );
					$wooApi         = new WoocommerceApi( $webshop->ID );
					$wooApi->post( "delete", "products/{$productChildId}", null );
					WoocommerceChildRelationships::deleteChildRelationship( $id, $webshop->ID, "product" );
				}
			}
		}
	}
}
