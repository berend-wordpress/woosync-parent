<?php

/**
 * @Author: ComSi
 * @Date: 9-11-17
 * @Time: 16:03
 */
class WooCommerceOrder {
	protected static $instance;

	public static function init() {
		is_null( self::$instance ) AND self::$instance == new self;


		return self::$instance;
	}

	public function __construct() {
		add_action( "woocommerce_reduce_order_stock", array( "WooCommerceOrder", "reduceStock" ) );

	}

	public static function reduceStock( $order ) {
		/** @var $order WC_Order */
		$items = $order->get_items();
		foreach ( $items as $item ) {
			/** @var  $item WC_Order_Item_Product */
			$productId = $item->get_product_id();
			$quantity  = $item->get_quantity();

			$sharedWebshops = WoocommerceChildRelationships::getSharedWebsites($productId);

			if(empty($sharedWebshops)) { continue; }

			foreach($sharedWebshops as $webshop) {
				$wooApi = new WoocommerceApi($webshop);

				$childProductId = WoocommerceChildRelationships::getChildRelationshipId($productId, $webshop, "product");


				$returnData = $wooApi->post("put", "products/" . $childProductId . "/" . $quantity . "/reducestock", null);

			}
		}
	}
}