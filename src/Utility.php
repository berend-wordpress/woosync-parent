<?php
/**
 * A collection of utility functions used by different classes.
 *
 * @Author: Berend de Groot
 * @Date: 10-8-17
 * @Time: 13:10
 */
defined( 'ABSPATH' ) OR exit;

class Utility {
	/**
	 * @param $attribute_name
	 *
	 * @return null|string
	 */
	public static function getWooAttrIdByName( $attribute_name ) {
		global $wpdb;
		$wooAttributeName = str_replace( "pa_", "", $attribute_name );
		$wooAttrId        = $wpdb->get_var( "SELECT attribute_id FROM " . $wpdb->prefix . "woocommerce_attribute_taxonomies WHERE attribute_name='{$wooAttributeName}'" );

		return $wooAttrId;
	}

	/**
	 * @param $term_id
	 *
	 * @return null|string
	 */
	public static function getTermNameById( $term_id ) {
		global $wpdb;
		$term_name = $wpdb->get_var( "SELECT name from " . $wpdb->prefix . "terms where term_id={$term_id}" );

		return $term_name;
	}

	/**
	 * @param $taxonomy_id
	 *
	 * @return null|string
	 */
	public static function getTaxonomyNameById( $taxonomy_id ) {
		global $wpdb;
		$attribute_name = $wpdb->get_var( "SELECT taxonomy FROM " . $wpdb->prefix . "term_taxonomy WHERE term_taxonomy_id='{$taxonomy_id}'" );

		return $attribute_name;
	}

	/**
	 * @param $name
	 *
	 * @return null|string
	 */
	public static function getTermIdByName( $name ) {
		global $wpdb;
		$term_id = $wpdb->get_var( "SELECT term_id from " . $wpdb->prefix . "terms where `name`='{$name}'" );

		return $term_id;
	}

	/**
	 * @param     $taxonomy_id
	 * @param     $type
	 * @param int $webshopId
	 *
	 * @return null|string
	 */
	public static function getChildAtrributeIdByParentTaxonomyId( $taxonomy_id, $type, $webshopId = 0 ) {
		$attribute_name = Utility::getTaxonomyNameById( $taxonomy_id );
		$wooAtrrId      = Utility::getWooAttrIdByName( $attribute_name );
		$wooChildAtrrId = WoocommerceChildRelationships::getChildRelationshipId( $wooAtrrId, $webshopId, $type );

		return $wooChildAtrrId;
	}

	public static function calculatePriceMarge( $oldPrice, $prijsMarge ) {
		$addToPrice = $oldPrice / 100 * $prijsMarge;
		$newPrice   = $oldPrice + $addToPrice;

		return $newPrice;
	}

	/**
	 * @param $post
	 * @param $get
	 *
	 * @return null
	 */
	public static function validateRequest( $post, $get ) {
		$postData = null;
		if ( ! empty( $post ) ) {
			$postData = $post;
		} else {
			if ( ! empty( $get ) ) {
				$postData = $get;
			}
		}

		return $postData;
	}
}