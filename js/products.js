jQuery( document ).ready(function($) {
    var $bulkAction =   $("#bulk-action-selector-top");

    $bulkAction.change(function() {
      if($bulkAction.val() === "push" || $bulkAction.val() === "cat_bluk_push") {
          $bulkAction.before("<select name='webshop' id='webshop'></select>");

          webshops.forEach(function(entry) {
             $("#webshop").append("<option value='" + entry.ID + "'>" + entry.post_title + "</option>");
          });
      }
    });
});