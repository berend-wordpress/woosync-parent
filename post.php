<?php
/**
 * We added one action to the row actions of post type Product, this file handles the action and maybe any other
 * actions which will be added later.
 */
require_once( "../../../wp-load.php" );
if ( current_user_can( "delete_others_posts" ) ) {
	if ( is_numeric( $_GET["post"] ) && $_GET["action"] == "reset" ) {
		$postId           = $_GET["post"];
		$sharedWebshopIds = WoocommerceChildRelationships::getSharedWebsites( $postId );
		foreach ( $sharedWebshopIds as $webshopId ) {
			WoocommerceFieldChanges::resetProduct( $webshopId, $postId );
			WoocommerceProduct::preparePostDataAndSavePost( array( "webshop" => $webshopId ), array( $postId ) );
		}
		wp_safe_redirect( "/wp-admin/edit.php?post_type=product" );
	}
}
