<?php
/**
 * Plugin Name: WooCommerce Product Sync (Parent)
 * Plugin URI: https://plugins.comsi.nl
 * Description: WooCommerce product synchronization
 * Version: 1.3.1
 * Author: ComSi
 * Author URI: https://plugins.comsi.nl
 */
require_once( "src/api/ChildApi.php" );
require_once( "src/Setup.php" );
require_once( "src/Utility.php" );
require_once( "src/entities/Webshop.php" );
require_once( "src/entities/WoocommerceFieldChanges.php" );
require_once( "src/entities/WoocommerceFieldChanges.php" );
require_once( "src/woocommerce/backend/WoocommerceProduct.php" );
require_once( "src/woocommerce/backend/WoocommerceAttribute.php" );
require_once( "src/woocommerce/backend/WoocommerceTerm.php" );
require_once( "src/woocommerce/frontend/WebshopsPage.php" );
require_once( "src/woocommerce/frontend/WoocommerceProductPage.php" );
require_once( "src/entities/WoocommerceChildRelationships.php" );
require_once( "src/woocommerce/frontend/WooCommerceCategories.php" );
require_once( "src/api/WoocommerceApi.php" );
require_once( "vendor/autoload.php" );
require_once("src/woocommerce/backend/WooCommerceOrder.php");
add_action( 'init', array( 'Setup', 'init' ) );
define( "PLUGIN_PATH", __DIR__ );
register_activation_hook( __FILE__, array( 'Setup', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'Setup', 'deactivate' ) );
register_uninstall_hook( __FILE__, array( 'Setup', 'uninstall' ) );
